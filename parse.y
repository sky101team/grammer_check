
%{
#include<unistd.h>
#include<stdio.h>   
#include "a.h"
#include "parse.tab.h"
int yylex(void);
int flag=1;
%}

%union{
struct ast* a;
double d;
}

%token  <a> IntConst FloatConst
%token <a> INT FLOAT RETURN IF ELSE WHILE Ident SPACE SEMI COMMA ASSIGNOP RELOP PLUS
MINUS STAR DIV AND OR  NOT LP RP LB RB LC RC AERROR   VOID EQOP MOD BREAK CONST CONTINUE
%token <a> EOL
%type  <a>Kw  error ConstInitllist FuncList BrrayDimList VarDefList BlockItemList InitValList ArrayDimList ConstDefList CompUnit Decl ConstDecl  ConstDef  ConstInitVal  VarDecl  Exp Cond
VarDef InitVal  FuncDef FuncType FuncFParams FuncFParam Block BlockItem  Stmt LVal PrimaryExp Number UnaryExp UnaryOp FuncRParams MulExp AddExp RelExp EqExp LAndExp LOrExp ConstExp


%right ASSIGNOP
%left FUNC_TYPE
%left OR
%left AND
%left RELOP EQOP MOD
%left PLUS MINUS
%left STAR DIV
%right NOT 
%left LP RP LB RB LC RC
%%
Kw: CompUnit{$$ = newast("Kw", 1, $1);}
CompUnit: CompUnit Decl {$$ = newast("CompUnit", 2, $1, $2);}
        | CompUnit FuncDef {$$ = newast("CompUnit", 2, $1,$2);}
        | Decl {$$ = newast("CompUnit", 1, $1);}
        | FuncDef {$$ = newast("CompUnit", 1, $1);};

Decl: ConstDecl {$$ = newast("Decl", 1, $1);}
    | VarDecl {$$ = newast("Decl", 1, $1);};

ConstDecl: CONST FuncType ConstDefList SEMI {$$ = newast("ConstDecl", 4,$1, $2, $3, $4);};

ConstDefList:
    ConstDef     {$$ = newast("ConstDefList", 1, $1);}
    |  ConstDef  COMMA ConstDefList {$$ = newast("ConstDefList", 3,$1, $2,$3);}
    ;

ConstDef:     Ident ArrayDimList  ASSIGNOP ConstInitVal{$$ = newast("ConstDef", 4,$1, $2, $3, $4);}
   ;
ArrayDimList:{$$ = newast("ArrayDimList",0,-1);}
    | LB ConstExp RB ArrayDimList  {$$ = newast("ArrayDimList", 4,$1, $2, $3, $4);}
    ;

ConstInitVal: ConstExp {$$ = newast("ConstInitVal", 1, $1);}
            | LC  RC {$$ = newast("ConstInitVal", 2, $1,$2);}
            | LC ConstInitllist RC {$$ = newast("ConstInitVal", 3,$1, $2,$3);}
;
ConstInitllist:
    ConstInitVal     {$$ = newast("ConstInitllist", 1, $1);}
    | ConstInitllist  COMMA ConstInitVal    {$$ = newast("ConstInitllist", 3,$1, $2,$3);}
    ;

VarDecl:  FuncType VarDefList SEMI {$$ = newast("VarDecl", 3,$1, $2,$3);};
      
VarDefList:
    VarDef {$$ = newast("VarDefList", 1, $1);}
    | VarDef COMMA VarDefList  {$$ = newast("VarDefList", 3,$1, $2,$3);}
    ;

VarDef:
    Ident ArrayDimList {$$ = newast("VarDef", 2, $1,$2);}
    | Ident ArrayDimList ASSIGNOP InitVal {$$ = newast("VarDef", 4,$1, $2, $3, $4);}
    ;

InitVal:
    Exp {$$ = newast("InitVal", 1, $1);}
    | LC InitValList RC {$$ = newast("InitVal", 3,$1, $2,$3);}
    ;

InitValList:{$$ = newast("InitValList",0,-1);}
    | InitVal {$$ = newast("InitValList", 1, $1);}
    | InitVal COMMA InitValList  {$$ = newast("InitValList", 3,$1, $2,$3);}
    ;

FuncDef: FuncType Ident LP RP Block {$$ = newast("FuncDef", 5, $1, $2,$3, $4, $5);}
             | FuncType Ident LP FuncFParams RP Block {$$ = newast("FuncDef", 6, $1, $2,$3, $4, $5,$6);}
             | VOID Ident LP RP Block {$$ = newast("FuncDef", 5, $1, $2,$3, $4, $5);}
             | VOID Ident LP FuncFParams RP Block {$$ = newast("FuncDef", 6, $1, $2,$3, $4, $5,$6);};


FuncType:  INT {$$ = newast("FuncType", 1, $1);}
    | FLOAT {$$ = newast("FuncType", 1, $1);};



FuncFParams: FuncFParam {$$ = newast("FuncFParams", 1, $1);}
            |  FuncFParam COMMA FuncFParams {$$ = newast("FuncFParams", 3, $1, $2,$3);};

FuncFParam:
     FuncType Ident {$$ = newast("FuncFParam", 2, $1,$2);}
    | FuncType Ident LB RB BrrayDimList {$$ = newast("FuncFParam", 5, $1, $2,$3, $4, $5);}    
;
BrrayDimList:{$$ = newast("BrrayDimList",0,-1);}
    | LB Exp RB BrrayDimList  {$$ = newast("BrrayDimList", 4,$1, $2, $3, $4);}
    ;

Block: LC BlockItemList RC {$$ = newast("Block", 3,$1, $2,$3);};

BlockItemList:{$$ = newast("BlockItemList",0,-1);}
    | BlockItemList BlockItem {$$ = newast("BlockItemList", 2, $1,$2);}
    ;

BlockItem:Decl {$$ = newast("BlockItem", 1, $1);}
         | error {flag=0;}
         | Stmt {$$ = newast("BlockItem", 1, $1);};

Stmt: LVal ASSIGNOP Exp SEMI {$$ = newast("Stmt", 4, $1, $2, $3,$4);}
  | SEMI {$$ = newast("Stmt", 1, $1);}
    | Exp SEMI {$$ = newast("Stmt", 2, $1,$2);}
    | Block {$$ = newast("Stmt", 1, $1);}
    | IF LP Cond RP Stmt  {$$ = newast("Stmt", 5, $1, $2, $3,$4,$5);}
    | IF LP Cond RP Stmt ELSE Stmt {$$ = newast("Stmt", 7, $1, $2, $3,$4,$5,$6, $7);}
    | WHILE LP Cond RP Stmt {$$ = newast("Stmt", 5, $1, $2, $3,$4,$5);}
    | BREAK SEMI {$$ = newast("Stmt", 2, $1,$2);}
    | CONTINUE SEMI {$$ = newast("Stmt", 2, $1,$2);}
    | RETURN Exp SEMI {$$ = newast("Stmt", 3, $1,$2,$3);}
    | RETURN SEMI {$$ = newast("Stmt",2, $1,$2);};

Exp: AddExp {$$ = newast("Exp", 1, $1);};

Cond: LOrExp {$$ = newast("Cond", 1, $1);};

LVal: Ident BrrayDimList{$$ = newast("LVal", 2, $1,$2);};

PrimaryExp: LP Exp RP {$$ = newast("PrimaryExp", 3, $1,$2,$3);}
           | LVal {$$ = newast("PrimaryExp", 1, $1);}
           | Number {$$ = newast("PrimaryExp", 1, $1);};

Number: IntConst {$$ = newast("Number", 1, $1);}
      | FloatConst {$$ = newast("Number", 1, $1);};

UnaryExp: PrimaryExp {$$ = newast("UnaryExp", 1, $1);}
         | Ident LP  RP {$$ = newast("UnaryExp", 3, $1,$2,$3);}
        | Ident LP FuncRParams RP {$$ = newast("UnaryExp", 4, $1,$2,$3,$4);}
        | UnaryOp UnaryExp {$$ = newast("UnaryExp", 2, $1, $2);};

UnaryOp: PLUS {$$ = newast("UnaryOp", 1, $1);}
       | MINUS {$$ = newast("UnaryOp", 1, $1);}
       | NOT {$$ = newast("UnaryOp", 1, $1);};

FuncRParams:  FuncList{$$ = newast("FuncRParams", 1, $1);}
          ;
FuncList:Exp {$$ = newast("FuncList", 1, $1);}
           | Exp COMMA FuncList{$$ = newast("FuncList", 3, $1,$2,$3);};


MulExp: UnaryExp {$$ = newast("MulExp", 1, $1);}
       | MulExp STAR UnaryExp {$$ = newast("MulExp", 3, $1,$2,$3);}
       | MulExp DIV UnaryExp {$$ = newast("MulExp", 3, $1,$2,$3);}
       | MulExp MOD UnaryExp {$$ = newast("MulExp", 3, $1,$2,$3);};

AddExp: MulExp {$$ = newast("AddExp", 1, $1);}
       | AddExp PLUS MulExp {$$ = newast("AddExp", 3, $1,$2,$3);}
       | AddExp MINUS MulExp {$$ = newast("AddExp",3, $1,$2,$3);};

RelExp: AddExp {$$ = newast("RelExp", 1, $1);}
       | RelExp RELOP AddExp {$$ = newast("RelExp", 3, $1,$2,$3);};

EqExp: RelExp {$$ = newast("EqExp", 1, $1);}
      | EqExp EQOP RelExp {$$ = newast("EqExp", 3, $1,$2,$3);};

LAndExp: EqExp {$$ = newast("LAndExp", 1, $1);}
        | LAndExp AND EqExp {$$ = newast("LAndExp", 3, $1,$2,$3);};

LOrExp: LAndExp {$$ = newast("LOrExp", 1, $1);}
       | LOrExp OR LAndExp {$$ = newast("LOrExp", 3, $1,$2,$3);};

ConstExp: AddExp {$$ = newast("ConstExp", 1, $1);};

%%