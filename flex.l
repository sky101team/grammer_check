%{
#include <stdio.h>
#include "stdlib.h"
#include "a.h"
#include "parse.tab.h"
%}
%option yylineno
FloatConst  {DecimalFloatConst}|{HexadecimalFloatConst}  
DecimalFloatConst  ([1-9][0-9]*|0)(\.[0-9]+)?([eE][+-]?[0-9]+)?[flFL]?|\.[0-9]+([eE][+-]?[0-9]+)?[flFL]?  
HexadecimalFloatConst (0[xX])([0-9a-fA-F]+(\.[0-9a-fA-F]*)?|\.[0-9a-fA-F]+)([pP][+-]?[0-9]+)?[flFL]?  
Digit [0-9]  
HexDigit [0-9a-fA-F]  
Sign [+-]
NUM [1-9]+[0-9]*|0
DN [0-9]+\.[0-9]+
NOND [a-zA-Z]
IntConst [1-9]+[0-9]*|[0][0-7]*|("0x"|"0X")[0-9a-fA-F]+
Ident ({NOND}|_)({NOND}|_|{NUM})*
RELOP    ">"|"<"|">="|"<="
EQOP        "=="|"!="
COMMENT1            "/*"[^*]*"*"+([^*/][^*]*"*"+)*"/"
COMMENT2            "//".*  
AERROR .
%%
"\n"         {}
"int"                 { yylval.a = newast("INT", 0, yylineno); return INT; }
"float"               { yylval.a = newast("FLOAT", 0, yylineno); return FLOAT; }
"void"                { yylval.a = newast("VOID", 0, yylineno); return VOID; }
"return"              { yylval.a = newast("RETURN", 0, yylineno); return RETURN; }
"if"                  { yylval.a = newast("IF", 0, yylineno); return IF; }
"else"                { yylval.a = newast("ELSE", 0, yylineno); return ELSE; }
"while"               { yylval.a = newast("WHILE", 0, yylineno); return WHILE; }
"break"               { yylval.a = newast("BREAK", 0, yylineno); return BREAK; }
"continue"            { yylval.a = newast("CONTINUE", 0, yylineno); return CONTINUE; }
"const"               { yylval.a = newast("CONST", 0, yylineno); return CONST; }
"="                    { yylval.a = newast("ASSIGNOP", 0, yylineno); return ASSIGNOP; }
{EQOP}               { yylval.a = newast("EQOP", 0, yylineno); return EQOP; }
{RELOP}              { yylval.a = newast("RELOP", 0, yylineno); return RELOP; }
"+"                    { yylval.a = newast("PLUS", 0, yylineno); return PLUS; }
"-"                     { yylval.a = newast("MINUS", 0, yylineno); return MINUS; }
"/"                     { yylval.a = newast("DIV", 0, yylineno); return DIV; }
"*"                     { yylval.a = newast("STAR", 0, yylineno); return STAR; }
"%"                     { yylval.a = newast("MOD", 0, yylineno); return MOD; }
"!"                      { yylval.a = newast("NOT", 0, yylineno); return NOT; }
"("                      { yylval.a = newast("LP", 0, yylineno); return LP; }
")"                      { yylval.a = newast("RP", 0, yylineno); return RP; }
"["                      { yylval.a = newast("LB", 0, yylineno); return LB; }
"]"                      { yylval.a = newast("RB", 0, yylineno); return RB; }
"{"                      { yylval.a = newast("LC", 0, yylineno); return LC; }
"}"                      { yylval.a = newast("RC", 0, yylineno); return RC; }
";"                      { yylval.a = newast("SEMI", 0, yylineno); return SEMI; }
","                      { yylval.a = newast("COMMA", 0, yylineno); return COMMA; }
"&&"                     { yylval.a = newast("AND", 0, yylineno); return AND; }
"||"                     { yylval.a = newast("OR", 0, yylineno); return OR; }
{Ident}               { yylval.a = newast("Ident", 0, yylineno); return Ident; }
{IntConst}           { yylval.a = newast("IntConst", 0, yylineno); return IntConst; }
{FloatConst}          { yylval.a = newast("FloatConst", 0, yylineno); return FloatConst; }
" "               {}
"\t"       {}
{COMMENT1}|{COMMENT2}   { }
{AERROR} { }

%%


int yywrap()
{
  return 1;
}
