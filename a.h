#ifndef YF_H
#define YF_H

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
extern int yylineno;//行号
extern char* yytext;//词
void yyerror(char *s,...);//错误处理函数

struct ast
{
    int line; //行号
    char* name;//语法单元的名字
    struct ast *l;//左孩子
    struct ast *r;//右孩子
    union
    {
    char* idtype;
     int intgr;
        float flt;
    };
};


struct ast *newast(char* name,int num,...);




#endif
