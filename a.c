#include <string.h>
# include<stdio.h>
# include<stdlib.h>
# include<stdarg.h>
# include"parse.tab.h"
# include"a.h"
int syntax_error = 0;
int i;
struct ast *yyparse_tree;
struct ast *newast(char* name,int num,...)//抽象语法树建立
{
    va_list valist; //定义变长参数列表
    struct ast *a=(struct ast*)malloc(sizeof(struct ast));//新生成的父节点
	//直接返回空值即可
    return a;
}
void yyerror(char*s,...) //变长参数错误处理函数
{
    va_list ap;
    va_start(ap,s); syntax_error = 1; 
    printf("Syntactical Error!\n");//错误行号
}
int main()
{
     yyparse(); 
 if (!syntax_error)
    {
       printf("Syntactical Correct!\n");
    }
return 0;
}
